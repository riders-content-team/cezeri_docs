#!/bin/bash
set -o errexit
set -e

source ${RIDERS_GLOBAL_PATH}/lib/helpers.sh
source /opt/ros/melodic/setup.bash
source "${RIDE_SOURCE_PATH}"/../devel/setup.bash
source ${HOME}/.nvm/nvm.sh;

# TODO: Check if 8080 fixed?
DEFAULT_GZ3D_URL=https://gz3d.riders.ai
GZ3D_URL=${GZ3D_URL:-${DEFAULT_GZ3D_URL}}

RIDE_GZWEB_ADDRESS=$(ride_ports_get_address 8080)
RIDE_GZWEB_URL=$(echo "$RIDE_GZWEB_ADDRESS" | sed -r -e 's/^https?\:\/\///g')

RIDE_ROSBRIDGE_ADDRESS=$(ride_ports_get_address 9090)
RIDE_ROSBRIDGE_URL=$(echo "$RIDE_ROSBRIDGE_ADDRESS" | sed -r -e 's/^https?\:\/\///g')

# ride_display_show_iframe "${GZ3D_URL}?gznode_url=${RIDE_GZWEB_URL}&rosbridge_url=${RIDE_ROSBRIDGE_URL}" "Web Simulator"
# ride_display_show_iframe "file://${RIDE_SOURCE_PATH}/test.html" "Web Simulator"
ride_display_show_iframe "file://${RIDE_SOURCE_PATH}/gz3d-embed/src/index.html" "Local Web Simulator"
sleep 1

GZNODE_DIR=/opt/gznode
ASSETS_DIR="${GZNODE_DIR}/http/client/assets"

# Fixes gazebo related issues
ride_ros_source_packages

# TODO: Write GZNode to PID FILE?
cd ${GZNODE_DIR}
sleep 1

# remove links created by yarn
rm -rf /tmp/yarn--*

# launch now
nvm use 10
npm run start