clear

GREEN='\033[0;92m'
NC='\033[0m' # No Color
printf "${GREEN}------------------------------------------------${NC}\n"
printf "${GREEN}Merhaba! Birazdan bazi yuklemeler gerceklesecek.${NC}\n"
printf "${GREEN}Yuklemelerin tamamlandiginda haber verecegiz.${NC}\n"
printf "${GREEN}-----------------------------------------------${NC}\n"

sleep 2

# URDF Loader NPM Install
source "${RIDERS_GLOBAL_PATH}/lib/helpers.sh"

#!/bin/bash
set -o errexit

# imports util functions
source ${RIDERS_GLOBAL_PATH}/lib/helpers.sh

# remove lib folders
cd ${RIDE_PACKAGES_PATH} && rm -rf ./*

# re-install dependencies
ride_packages_clone_all

# rebuild ros packages
ride_ros_catkin_make

# install requests package
python -m pip install requests

# Source assets for Gzweb

source "${RIDE_SOURCE_PATH}"/../devel/setup.bash
GZNODE_DIR=/opt/gznode
ASSETS_DIR="${GZNODE_DIR}/http/client/assets"


# Fixes gazebo related issues
ride_ros_source_packages


# Link models to the assets folder
printf "Copying local resources to Web Renderer\n"
$GZNODE_DIR/get_local_models.py $ASSETS_DIR

printf "Webifying local resources & models\n"
$GZNODE_DIR/webify_models_v2.py $ASSETS_DIR
printf "Linking local resources"
rospack list | awk '{print $2}' | xargs -n 1 -I {} ln -sf {} "${ASSETS_DIR}"
ride_show_riders_commands
ride_ui_resize_panel bottom 150

clear

GREEN='\033[0;92m'
NC='\033[0m' # No Color
printf "${GREEN}-----------------------${NC}\n"
printf "${GREEN}Yuklemeler tamamlandi. ${NC}\n"
printf "${GREEN}-----------------------${NC}\n"


