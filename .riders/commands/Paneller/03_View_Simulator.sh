#!/bin/bash
set -o errexit
set -e

source ${RIDERS_GLOBAL_PATH}/lib/helpers.sh

ride_display_show_iframe "file://${RIDE_SOURCE_PATH}/gz3d-embed/src/index.html" "Local Web Simulator"
sleep 1