#!/bin/bash
set -o errexit
set -e

source ${RIDERS_GLOBAL_PATH}/lib/helpers.sh
source /opt/ros/melodic/setup.bash
source "${RIDE_SOURCE_PATH}"/../devel/setup.bash

set +e
rosnode kill rosbridge_websocket
rosnode kill gazebo
set -e

# Starts Xvfb & OpenVNC server
export DISPLAY=":0"
ride_display_start "${DISPLAY}"

# start ros
source /opt/ros/melodic/setup.bash
source "${RIDE_SOURCE_PATH}"/../devel/setup.bash

# Fixes gazebo related issues
ride_ros_source_packages

# TODO: We need to automatize this
export GAZEBO_MODEL_DATABASE_URI=""

# create roslaunch file in ${WORKSPACE}/.riders/start.launch
ROSLAUNCH_FILE_PATH=${RIDE_SOURCE_RIDERS_PATH}/start.launch

# ride_ros_configuration_to_roslaunch ${ROSLAUNCH_FILE_PATH}

# we should run other tasks here
ride_tasks_run_file() {
    NAME="${1}"
    FULL_PATH="${2}"
    UUIDPATH=$(ride_ipc_generate_path)
    printf '{"id": "riders.tasks.run", "args": { "name": "%s", "fullPath": "%s", "forceRestart": false, "showTerminal": true }}' "$1" "$2" > $UUIDPATH
}

ride_tasks_run_file_background() {
    NAME="${1}"
    FULL_PATH="${2}"
    UUIDPATH=$(ride_ipc_generate_path)
    printf '{"id": "riders.tasks.run", "args": { "name": "%s", "fullPath": "%s", "forceRestart": true, "showTerminal": false }}' "$1" "$2" > $UUIDPATH
}

ride_tasks_run_file_background "03_View_Simulator_Start.sh" ${RIDE_SOURCE_PATH}/.riders/03_View_Simulator_Start.sh

sleep 2

ride_tasks_run_file_background "04_User_Code.sh" ${RIDE_SOURCE_PATH}/.riders/commands/Paneller/04_User_Code.sh
sleep 1
ride_tasks_run_file_background "07_Diagnostic_Panel.sh" ${RIDE_SOURCE_PATH}/.riders/commands/Paneller/07_Diagnostic_Panel.sh
sleep 1
ride_ui_resize_panel bottom 0
ride_ui_resize_panel left 0
roslaunch "${ROSLAUNCH_FILE_PATH}"