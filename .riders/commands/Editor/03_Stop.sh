#!/bin/bash
set -o errexit
set -e

source ${RIDERS_GLOBAL_PATH}/lib/helpers.sh

set +e
rosnode kill adaptive_evaluator
rosnode kill user_code
clear
set +e

killall -9 rosmaster
killall -9 roslaunch
killall gzserver

ride_display_close_iframe "file://${RIDE_SOURCE_PATH}/gz3d-embed/src/index.html"
sleep 1
ride_display_close_iframe "file://${RIDE_SOURCE_PATH}/.riders/diagnostik_panel.html"
sleep 0.5
ride_ui_resize_panel left 0