#!/bin/bash
set -o errexit
set -e

# This is optional
source ${RIDERS_GLOBAL_PATH}/lib/helpers.sh

# start ros
source /opt/ros/melodic/setup.bash
source "${RIDE_SOURCE_PATH}"/../devel/setup.bash

# Fixes gazebo related issues
ride_ros_source_packages

rosservice call /gazebo/reset_simulation

set +e
rosnode kill adaptive_evaluator
rosnode kill user_code
clear
set +e

# create reset.launch file ride roslaunch ...
RESET_LAUNCH_FILE_PATH=${RIDE_SOURCE_RIDERS_PATH}/reset.launch

# ride create-reset-launch "${RIDE_CONFIGURATION_PATH}" "${RESET_LAUNCH_FILE_PATH}"

# TODO: delete existing gazebo resources
# loop over models in configuration
ROBOT_MODELS=$(yq e -j ${RIDE_CONFIGURATION_PATH} | jq '.runtime.robots | keys | .[]')
for model in $ROBOT_MODELS; do
    rosservice call /gazebo/delete_model ${model}
done

# Cem: The following /gazebo/reset_simulation call is necessary in Theia editor. VS Code works without it.
rosservice call /gazebo/unpause_physics

ride_ui_resize_panel bottom 0
ride_ui_resize_panel left 0


# finally start roslaunch with same names, which will kill old instances
roslaunch "${RESET_LAUNCH_FILE_PATH}" 

sleep 1





